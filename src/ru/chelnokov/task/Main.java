package ru.chelnokov.task;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import ru.chelnokov.threads.MediaThread;

import java.io.*;

/**
 * Класс, позволяющий параллельно скачать файл формата .jpg  и файл формата .mp3
 * музыка воспроизводится
 *
 * @author Chelnokov E.I. 16it18k
 */
public class Main {
    private static final String IN_FILE_TXT = "src\\ru\\chelnokov\\files\\inputFile.txt";

    public static void main(String[] args) throws FileNotFoundException, JavaLayerException {
        MediaThread picture = null;
        MediaThread audio = null;
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT))) {
            String urlAndPath;
            if ((urlAndPath = inFile.readLine()) != null) {
                String[] data = urlAndPath.split(" ");
                picture = new MediaThread(data[0], data[1]);

                urlAndPath = inFile.readLine();
                data = urlAndPath.split(" ");
                audio = new MediaThread(data[0], data[1]);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        picture.setName("SAO.jpg");
        audio.setName("MYTH & ROID - HYDRA.mp3");

        picture.start();
        audio.start();
        try {
            audio.join();
        } catch (InterruptedException e) {
            System.out.println("Ошибка загрузки файла");
        }

        FileInputStream inputStream = new FileInputStream(audio.getPath() + audio.getName());
        Player player = new Player(inputStream);
        player.play();
    }

}
