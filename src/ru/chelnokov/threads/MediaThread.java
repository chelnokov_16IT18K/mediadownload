package ru.chelnokov.threads;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class MediaThread extends Thread{
    private String url;//ссылка на файл
    private String path;//адрес для загрузки

    public MediaThread(String url, String path) {
        this.url = url;
        this.path = path;
    }

    /**
     * метод для запуска скачивания
     *
      */

    @Override
    public void run() {
        try {
            downloadUsingNIO(url, path + getName());
            System.out.println("Файл " + getName() + " скачан успешно");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    /**
     * скачивает файл
     *
     * @param strUrl ссылка на файл
     * @param file   полное имя файла
     * @throws IOException ну что-то там, извините
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }

    /**
     * возвращает путь к файлу
     * @return путь к файлу
     */
    public String getPath() {
        return path;
    }

}
